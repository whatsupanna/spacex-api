import Vue from 'vue'
import App from './App.vue';

Vue.config.productionTip = false

// // include css files
// import '../public/style'

new Vue({
  render: h => h(App),
}).$mount('#app')