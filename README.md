# spacex-api-exercise

## To Get Setup:

1. if you're happy and you know it clone the repo
1. open `src/client/spacex-client` in the terminal
1. run `npm install`
1. run `npm run build`
1. navigate to root project directory in terminal
1. run `npm install`
1. run `npm run start`

### Requirements:

- node.js v8.12.0+
