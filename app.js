import express from "express";
import bodyParser from "body-parser";
import path from "path";

import sqlite3 from "sqlite3";

// Set up the express app
const app = express();


const relevantLaunchData = [
    "flight_number",
    "mission_name",
    "launch_success",
    "launch_date_utc",
];

const relevantRocketData = [
    "rocketrocket_id",
    "rocketrocket_name",
];

app.use(express.static(path.join(__dirname + "/src/client/spacex-client/dist/")));

// Parse incoming requests data
app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: false
    })
);

const generateSelectStatement = (relevantData) => {
    let statement = "SELECT";

    relevantData.map((item, index) => {
        if (index < relevantData.length - 1) {
            statement += ` ${item},`;
        } else {
            statement += ` ${item}`;
        }
    });

    return statement;
};

// open the database
let db = new sqlite3.Database(
    "./data/spacex.db",
    sqlite3.OPEN_READWRITE,
    (err) => {
        if (err) {
            console.error(err.message);
        }
        console.log("Connected to the SpaceX launch database.");
    }
);

// db.close(err => {
//     if (err) {
//         console.error(err.message)
//     }
//     console.log('Close the database connection.')
// })

app.get("/", function (req, res) {
    res.sendFile(path.join(__dirname + "/src/client/spacex-client/dist/index.html"));
});

// get specific rocket
app.get("/api/v1/launch/:rocket_id", (req, res) => {
    const rocket_id = req.params.rocket_id;
    let sql = `SELECT rocketrocket_id, rocketrocket_name
    FROM mytable
    WHERE rocketrocket_id = '${rocket_id}'`;

    db.all(sql, [], (err, rows) => {
        if (err) {
            throw err;
        }

        if (rows && rows.length > 0) {
            res.status(200).send({
                success: "true",
                message: "launch data by rockiet id retrieved successfully",
                launches: rows
            });
        } else {
            // return all rockets
            let sql = `
                ${generateSelectStatement(relevantRocketData)}
                FROM mytable`;

            db.all(sql, [], (err, rows) => {
                if (err) {
                    throw err;
                }
                res.status(200).send({
                    success: "true",
                    message: "specific rocket not found, return all",
                    launches: rows
                });
            });
        }
    });
});

// get all launch info
app.get("/api/v1/launchs", (req, res) => {
    let sql = `
		${generateSelectStatement(relevantLaunchData)}
		FROM mytable`;

    db.all(sql, [], (err, rows) => {
        if (err) {
            throw err;
        }
        res.status(200).send({
            success: "true",
            message: "all launch retrieved successfully",
            launches: rows
        });
    });
});

// update launch success info
app.post("/api/v1/update/:flight_number", (req, res) => {
    const flight_number = req.params.flight_number;
    let sql = `UPDATE mytable
			SET launch_success = NOT launch_success
            WHERE flight_number = ${flight_number}`;

    db.run(sql, [], (err) => {
        if (err) {
            return console.error(err.message);
        }
        res.status(200).send({
            success: "true",
            message: "launch data updated successfully"
        });
    });
});

const PORT = 3000;

app.listen(PORT, () => {
    console.log(`server running on port ${PORT}`);
});